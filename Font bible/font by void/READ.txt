#Font mod by void@median-xl.com

#Using the font with D2SE
Put "data" into the mod folder you want to use it with.
a) In the mod folder, open "D2SE_SETUP" and change "Direct=0" to "Direct=1".
b) If "Direct" doesn't exist, just add "Direct=1" and "Txt=0" under "[USERSETTINGS]" and "Modable=1" under "[Protected]".
Save changes.

#Using the font if you don't have D2SE
Put "data" in the the game folder (the folder where you can also find the "save" folder.
Open your Diablo 2 shortcut and add to the very end of the path without the quotation marks: " -direct" (notice the space before -direct). Save changes.
# Diablo 2 Fonts

Repository with fonts, tools and a guide to make them.

Feel free to contribute!

![alt text](https://i.imgur.com/gkgFXR7.png)
![alt text](https://i.imgur.com/W6Io4lE.png)

# How to install a specific font

* Create appropriate folders so that folder tree looks like this : .../yourD2Install/data/local/font/latin

* Place __font16.tbl__ and __font16.dc6__ (and other tbl/dc6 pairs if present) in the latin folder

* Create a shortcut to the game executable and add the __-direct__ command

* Enjoy that font in your Diablo 2 game
DC6FONT : freeware, Paul Siramy, 08 November 2011
=======

This program allow to make new fonts in the game.

Launch "exemple 1, make pcx.bat", and you'll have a new
file : "font42.pcx". This will show the font, in a 16 * 16
characters table. The dimensions of one character are written
in the console window, right before the "press any key" message.

   making_pcx...ok : each character is 39 * 41 pixels

so when you open font42.pcx in Paint Shop Pro, you can set
the Grid to 39 * 41 (in the window File | Preferences |
General Menu Preference | Ruler and Units), then "View Grid"
(or Ctrl+Alt+G), and you'll see usufull lines.

Apparently, the size of each character is hardcoded, so don't
try to make new characters of another size of the original. In
our case, the 'A' character in "font42.pcx" is 18*18 pixels, so
don't make a new one bigger nor smaller, try to respect the
original width & height of each character.



Now, if you launch "exemple 2, make dc6.bat", you'll have a new
font16.dc6, and if you look carefuly, you'll see that the
digit '5' is much readable than the original. The console window
will tell you that the characters are 14 * 16 pixels each.

   Good .pcx font, each character is 14 * 16 pixels
   making_dc6...

Latin fonts are the dc6 in the data\global\local\font\latin\
directory of d2data.mpq.

If you check the source of the program, you'll see a quick &
dirty programing style. At least it works.

Enjoy

Edit (05/04/2020) by random user : You can drag and drop a file
to the .exe . A pcx will make a dc6 with the same name, same goes
for the pcx, it will make you your dc6.

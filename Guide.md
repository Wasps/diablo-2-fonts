# How to make fonts for Diablo II quickly and without any Kerfuffle

This will apply for font16 but you can use this guide for all the others

## The Fast way

This method will yield you 4 font sizes in an instant, but also allow some flexibility if you want your font to fit better.
It will allow you to avoid any palette headaches, PCX conversions, having 4/5 programs open at all times, etc.

Here is some detailed instructions :   

- Install both Python 3.8+ and Pillow 8+ on your computer.
- Then download the FontMaker folder in the repo's script folder. It has a python script that will give you automatic font24/16/8/6 bitmaps as BMP images.
- Download the font (.OTF/.TTF) you want to use and place it into your directory.
- Modify the Python script to point out to your fonts name. Modify the end if you also need other fonts or not.
- Run the script or bat file. You can also modify the values if you need so (font size, offsets, what chars to display, etc).

Then open D2FontEditor.exe in the tool folder. Press F8 (edit/import tab) and import you newly made BMP 
font files. Then modify the settings to align the font to the grid. The settings of the program won't make sense that
much, because they are not well labeled. Here is a recap of the settings needed for each fonts:

| font name 	| font width 	| font Height 	|
|:---------:	|:----------:	|:-----------:	|
| 24        	| 21         	| 25          	|
| 16        	| 13         	| 15          	|
| 8         	| 14         	| 13          	|
| 6         	| 8          	| 10          	|

Other settings such as 'Left offset' and 'Top offset' has to be set to 0, while 'Grid width' and 'border height' as 1. those settings works for all the fonts. You can check those settings in the setting.png image.

Then, press OK and you can fix your offset with the program itself. One good thing, is that, generally,
offsets from one font works for the other too.   
Export both Dc6 and offsets and you are set.

Check the end of this guide to get additional infos.  
The following ways are slowers but dont require python nor pillow.

## The slow way

### Choosing a font and exporting it as BMP

Get a font from anywhere you want, __make sure it includes diacritics/accents__.

Install that font to your computer.

On the Internet you can find tools to convert otf/ttf fonts to bitmap.
My Preferred one being Bitmap Font Builder.

Open your font with it, set the foreground to a specific d2 palette RGB value and change font size to match the font you want to make.

Align the font to the left, or else you will have issues. Font smoothing option is pretty much mandatory in most cases.

You can then "Save as BMP".

[Useful pics](https://imgur.com/a/yJqIoRR)

### Making a PCX file

Converting your BMP to PCX is rather easy with tools like "Paint.net" or "The GIMP"

Note : GIMP can embed a palette into a PCX, it requires some work which isn't in the scope of this guide but shortens the font-making process a bit, especially when doing repeated edits.

__This is a good time to make adjustments__ to your PCX, whether it is character tweaks, or color adjustments.

I use Paint.net with 'recolor using palette' plugin [(Found here)](https://forums.getpaint.net/topic/111468-recolor-using-palette/) in order for my font to be later displayable by the game using unit palette.

A plugin to export to PCX may also be needed. [(Found here)](https://forums.getpaint.net/topic/2135-pcx-plug-in/)

### Here's the greyscale portion of UNITS palette : 

These, in most cases, are the only colours you will use when making Diablo II fonts. The unit palette actually has 256 colors, of which 226 are uniques.

|        |  R  |  G  |  B  |
|--------|:---:|:---:|:---:|
| White  | 244 | 244 | 244 |
| Grey 0 | 196 | 196 | 196 |
| Grey 1 | 148 | 152 | 156 |
| Grey 2 | 132 | 132 | 132 |
| Grey 3 | 100 | 100 | 100 |
| Grey 4 |  72 |  72 |  72 |
| Grey 5 |  56 |  56 |  56 |
| Grey 6 |  44 |  44 |  44 |
| Grey 7 |  28 |  28 |  28 |
| Black  |  0  |  0  |  0  |

Note : 
I don't really know if using a white 255 255 255 makes a lot of difference. If someone feels the need to add other colors please do, maybe compile them in another txt/md file.

### Embedding the UNITS palette in your PCX and converting it to DC6

Launch Dc6Creator, on the left set the active palette to "units" before doing anything, then "Import" your pcx, and "Export" it back as a pcx, the process essentially embeds Units palette in the image.

__This is your base file now__. You will only need to modify it if you need to make adjustments.
Now and every time you make any modification to your pcx, you need to convert it to a dc6 with Paul Syramy's "dc6 font" tool. 

Tip : just drag and drop your pcx/dc6 to the exe to convert one way or the other, no need to make a BAT file for each.

### The miraculous font editor : Making the actual Diablo II font file

Thanks to Gamemaster's D2FontEditor (tools) you can very easily make a font out of your dc6.
Indeed, a dc6 font need both a "multi-frame" dc6 (each character is contained in a "frame") and a offsets table in which kerning (space between characters) is saved.

Simply start the program, make a new d2font file, and import your font image dc6. Now you can adjust offsets, preview your font.

Sometimes you might come across some graphical bugs with your font, for instance random dots all over the character space. 

[like this](https://imgur.com/a/CoU2juq)

This is because you have some characters bleeding onto their neighbors. Move your characters around in their alloted spaces or simply delete those pixels to fix the issue, it is your call.

The square preview on the right has red lines indicating offsets, click and drag them around to adjust kerning.

Now you can export both the final dc6 and it's offsets table (which oddly enough is tbl file, not to be confused with the kind that governs in-game text strings). 

# Additional info

## Different font sizes and their uses within Diablo II

__font16__ is used for most text, including: item names and description, char stat values, skills texts, etc

__font8__ is used for smaller text like big defense/stamina/life values

__font6__ is used for even smaller text like stat names and big attack damage/rating values in stats pane

__fontingamechat__ is used for in-game chats, and npc text (I believe)

Note : I have limited knowledge about this, feel free to complement.

## Expected BMP font and chararacter size in pixels
As a rule of thumb, Char Width and Height are the corresponding image's size divided by 16. This is because the font in itself is 16x16 glyphs.  

|                | Image Size | Char Width | Char Height |
|:--------------:|:----------:|:----------:|:-----------:|
|     font 6     |   144x176  |      9     |      11     |
|     font 8     |   240x224  |     15     |      14     |
|     font 16    |   224x256  |     14     |      16     |
|     font 24    |   352x416  |     22     |      26     |
|     font 30    |   464x480  |     29     |      30     |
|     font 42    |   624x656  |     39     |      41     |
| fontingamechat |   256x240  |     16     |      15     |

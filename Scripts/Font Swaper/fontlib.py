'''Library of functions

'''
#imports----------------------------------------
try:
    from configparser import ConfigParser
except ImportError:
   from ConfigParser import ConfigParser  # ver. < 3.0

import os
from os import walk
from pathlib import Path
import shutil
import distutils.dir_util


# Get paths from the INI---------------------
config = ConfigParser()

# parse setup.ini and retrieve user input
config.read('setup.ini')
fpath = config.get('PATH', 'fpath')
mpath = config.get('PATH', 'mpath')
ext = config.get('EXTENSIONS', 'ext')
gfdict ={}

def menu():
	fdictionary = {}
	initfolder()
	a = getfolderlist()
	print("which font do you want to install?")
	c = 1
	for item in a:
		#print(item)
		if folderispopulated(str(fpath) + item) and folderisvalid(str(fpath) + item):
			print(str(c) + " " +  item)
			fdictionary[c] = item
			
			c+=1
	return fdictionary
	


def getfolderlist():
	# initialize a list
	f = []
	directorynames = []
	# itterate all the font folders in the folder containing fonts
	for (dirpath, dirnames, filenames) in walk(fpath):
		f.extend(dirnames)
		break
		
	for i in range(len(f)):
		break
	
	return f

def initfolder():
	if not os.path.isdir(mpath):
		os.mkdir(mpath)

def folderispopulated(item):
	if len(os.listdir(item) ) == 0:
		#print("return 0 achieved")
		return 0
	else:
		return 1

def folderisvalid(liste):
	# check if the folder has both a .dc6 and tbl, cant manage to make this work..
	return 1

def finput(dict, input):
	print("Copying " + dict.get(int(input)))
	return dict.get(int(input))

def copytofinput(font):
	#shutil.copy(fpath + str(font), mpath)
	deletefonts()
	for files in os.listdir(fpath + str(font)):
		#print(files)
		#print(ext)
		# broken for some obscure reasons ( i have the same exact code in another script and it works..)
		# if files.endswith(ext):
		# 	print("lol")
		# 	shutil.copy(fpath + str(font) +"\\" +  str(files), mpath)
		break
	distutils.dir_util.copy_tree(fpath + font, mpath)

def deletefonts():
	for filename in os.listdir(mpath):
		filepath = os.path.join(mpath, filename)
		try:
			shutil.rmtree(filepath)
		except OSError:
			 os.remove(filepath)

